USE [master]
GO
/****** Object:  Database [YHFrame]    Script Date: 02/27/2017 19:38:32 ******/
CREATE DATABASE [YHFrame] ON  PRIMARY 
( NAME = N'YHFrame', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\YHFrame.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'YHFrame_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\YHFrame_log.LDF' , SIZE = 576KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [YHFrame] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [YHFrame].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [YHFrame] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [YHFrame] SET ANSI_NULLS OFF
GO
ALTER DATABASE [YHFrame] SET ANSI_PADDING OFF
GO
ALTER DATABASE [YHFrame] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [YHFrame] SET ARITHABORT OFF
GO
ALTER DATABASE [YHFrame] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [YHFrame] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [YHFrame] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [YHFrame] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [YHFrame] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [YHFrame] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [YHFrame] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [YHFrame] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [YHFrame] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [YHFrame] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [YHFrame] SET  ENABLE_BROKER
GO
ALTER DATABASE [YHFrame] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [YHFrame] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [YHFrame] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [YHFrame] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [YHFrame] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [YHFrame] SET READ_COMMITTED_SNAPSHOT ON
GO
ALTER DATABASE [YHFrame] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [YHFrame] SET  READ_WRITE
GO
ALTER DATABASE [YHFrame] SET RECOVERY FULL
GO
ALTER DATABASE [YHFrame] SET  MULTI_USER
GO
ALTER DATABASE [YHFrame] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [YHFrame] SET DB_CHAINING OFF
GO
USE [YHFrame]
GO
/****** Object:  Table [dbo].[User]    Script Date: 02/27/2017 19:38:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[account] [nvarchar](50) NOT NULL,
	[pwd] [nvarchar](256) NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[sex] [int] NOT NULL,
	[email] [nvarchar](50) NULL,
	[phone] [nvarchar](20) NULL,
	[enabled] [bit] NOT NULL,
	[pwdErrorCount] [int] NOT NULL,
	[loginCount] [int] NOT NULL,
	[Icon] [nvarchar](256) NULL,
	[createId] [int] NULL,
	[createBy] [nvarchar](max) NULL,
	[createTime] [datetime] NULL,
	[modifyId] [int] NULL,
	[modifyBy] [nvarchar](max) NULL,
	[modifyTime] [datetime] NULL,
	[order] [int] NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'account'
GO
/****** Object:  Table [dbo].[Role]    Script Date: 02/27/2017 19:38:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[describe] [nvarchar](200) NULL,
	[createId] [int] NULL,
	[createBy] [nvarchar](max) NULL,
	[createTime] [datetime] NULL,
	[modifyId] [int] NULL,
	[modifyBy] [nvarchar](max) NULL,
	[modifyTime] [datetime] NULL,
	[order] [int] NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permssion]    Script Date: 02/27/2017 19:38:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permssion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[pid] [int] NOT NULL,
	[PermssionType] [int] NOT NULL,
	[describe] [nvarchar](max) NULL,
	[method] [int] NOT NULL,
	[createId] [int] NULL,
	[createBy] [nvarchar](max) NULL,
	[createTime] [datetime] NULL,
	[modifyId] [int] NULL,
	[modifyBy] [nvarchar](max) NULL,
	[modifyTime] [datetime] NULL,
	[url] [nvarchar](max) NOT NULL,
	[area] [nvarchar](max) NULL,
	[controller] [nvarchar](max) NULL,
	[action] [nvarchar](max) NULL,
	[icon] [nvarchar](max) NULL,
	[order] [int] NULL,
 CONSTRAINT [PK_dbo.Permssions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 02/27/2017 19:38:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[pid] [int] NOT NULL,
	[permssionType] [int] NOT NULL,
	[describe] [nvarchar](max) NULL,
	[createId] [int] NULL,
	[createBy] [nvarchar](max) NULL,
	[createTime] [datetime] NULL,
	[modifyId] [int] NULL,
	[modifyBy] [nvarchar](max) NULL,
	[modifyTime] [datetime] NULL,
	[order] [int] NULL,
	[url] [nvarchar](100) NULL,
 CONSTRAINT [PK_dbo.Menus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Department]    Script Date: 02/27/2017 19:38:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Department](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[createId] [int] NULL,
	[createBy] [nvarchar](max) NULL,
	[createTime] [datetime] NULL,
	[modifyId] [int] NULL,
	[modifyBy] [nvarchar](max) NULL,
	[modifyTime] [datetime] NULL,
	[name] [nvarchar](50) NOT NULL,
	[order] [int] NULL,
 CONSTRAINT [PK_dbo.Departments] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User_Role]    Script Date: 02/27/2017 19:38:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[role_id] [int] NOT NULL,
	[createId] [int] NULL,
	[createBy] [nvarchar](max) NULL,
	[createTime] [datetime] NULL,
	[modifyId] [int] NULL,
	[modifyBy] [nvarchar](max) NULL,
	[modifyTime] [datetime] NULL,
	[order] [int] NULL,
 CONSTRAINT [PK_dbo.User_Role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_role_id] ON [dbo].[User_Role] 
(
	[role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_user_id] ON [dbo].[User_Role] 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Default [DF__Permssions__url__0BC6C43E]    Script Date: 02/27/2017 19:38:33 ******/
ALTER TABLE [dbo].[Permssion] ADD  DEFAULT ('') FOR [url]
GO
/****** Object:  Default [DF__Department__name__0AD2A005]    Script Date: 02/27/2017 19:38:33 ******/
ALTER TABLE [dbo].[Department] ADD  DEFAULT ('') FOR [name]
GO
/****** Object:  ForeignKey [FK_dbo.User_Role_dbo.Roles_role_id]    Script Date: 02/27/2017 19:38:33 ******/
ALTER TABLE [dbo].[User_Role]  WITH CHECK ADD  CONSTRAINT [FK_dbo.User_Role_dbo.Roles_role_id] FOREIGN KEY([role_id])
REFERENCES [dbo].[Role] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[User_Role] CHECK CONSTRAINT [FK_dbo.User_Role_dbo.Roles_role_id]
GO
/****** Object:  ForeignKey [FK_dbo.User_Role_dbo.Users_user_id]    Script Date: 02/27/2017 19:38:33 ******/
ALTER TABLE [dbo].[User_Role]  WITH CHECK ADD  CONSTRAINT [FK_dbo.User_Role_dbo.Users_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[User_Role] CHECK CONSTRAINT [FK_dbo.User_Role_dbo.Users_user_id]
GO
