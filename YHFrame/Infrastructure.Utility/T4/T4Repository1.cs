﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Core.Interface;
using Model.Base;
using System.ComponentModel.Composition;

namespace Repository.Core
{
	
[Export(typeof(IDepartmentRepository))]
  public partial class DepartmentRepository : BaseRepository<Department>,IDepartmentRepository
    {
    }
       	
[Export(typeof(IMenuRepository))]
  public partial class MenuRepository : BaseRepository<Menu>,IMenuRepository
    {
    }
       	
[Export(typeof(IPermssionRepository))]
  public partial class PermssionRepository : BaseRepository<Permssion>,IPermssionRepository
    {
    }
       	
[Export(typeof(IRoleRepository))]
  public partial class RoleRepository : BaseRepository<Role>,IRoleRepository
    {
    }
       	
[Export(typeof(IUserRepository))]
  public partial class UserRepository : BaseRepository<User>,IUserRepository
    {
    }
       	
[Export(typeof(IUser_RoleRepository))]
  public partial class User_RoleRepository : BaseRepository<User_Role>,IUser_RoleRepository
    {
    }
       

}
