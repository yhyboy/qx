﻿using System;
using System.Collections.Generic;
using System.Linq;
using  Model.Base;
using  Model;
using System.ComponentModel.Composition;
namespace Repository.Core.Interface
{
	
		   // [InheritedExport]
 public  partial interface IDepartmentRepository : IRepository<Department>
    {
    }
       	
		   // [InheritedExport]
 public  partial interface IMenuRepository : IRepository<Menu>
    {
    }
       	
		   // [InheritedExport]
 public  partial interface IPermssionRepository : IRepository<Permssion>
    {
    }
       	
		   // [InheritedExport]
 public  partial interface IRoleRepository : IRepository<Role>
    {
    }
       	
		   // [InheritedExport]
 public  partial interface IUserRepository : IRepository<User>
    {
    }
       	
		   // [InheritedExport]
 public  partial interface IUser_RoleRepository : IRepository<User_Role>
    {
    }
       }
