﻿using System;
using System.Collections.Generic;
using System.Linq;
using  Model.Base;
using  Model;
namespace Domain.Core.Interface
{
	

 public  partial interface IDepartmentManager : IBaseManager<Department>
    {
    }
       	

 public  partial interface IMenuManager : IBaseManager<Menu>
    {
    }
       	

 public  partial interface IPermssionManager : IBaseManager<Permssion>
    {
    }
       	

 public  partial interface IRoleManager : IBaseManager<Role>
    {
    }
       	

 public  partial interface IUserManager : IBaseManager<User>
    {
    }
       	

 public  partial interface IUser_RoleManager : IBaseManager<User_Role>
    {
    }
       }
