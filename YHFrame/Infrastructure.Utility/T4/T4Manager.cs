﻿using System;
using System.Collections.Generic;
using System.Linq;
using  Model.Base;
using  Model;
using Domain.Core.Interface;
using System.ComponentModel.Composition;
using Repository.Core.Interface;
using Repository.Core;
namespace Domain.Core
{
	
 [Export(typeof(IDepartmentManager))]
 public  partial class DepartmentManager : BaseManager<Department>,IDepartmentManager
    {

	        [ImportingConstructorAttribute]
		      public DepartmentManager( IDepartmentRepository Repository)
		      {
                  base.GetRepository(Repository);
		      }
    }
       	
 [Export(typeof(IMenuManager))]
 public  partial class MenuManager : BaseManager<Menu>,IMenuManager
    {

	        [ImportingConstructorAttribute]
		      public MenuManager( IMenuRepository Repository)
		      {
                  base.GetRepository(Repository);
		      }
    }
       	
 [Export(typeof(IPermssionManager))]
 public  partial class PermssionManager : BaseManager<Permssion>,IPermssionManager
    {

	        [ImportingConstructorAttribute]
		      public PermssionManager( IPermssionRepository Repository)
		      {
                  base.GetRepository(Repository);
		      }
    }
       	
 [Export(typeof(IRoleManager))]
 public  partial class RoleManager : BaseManager<Role>,IRoleManager
    {

	        [ImportingConstructorAttribute]
		      public RoleManager( IRoleRepository Repository)
		      {
                  base.GetRepository(Repository);
		      }
    }
       	
 [Export(typeof(IUserManager))]
 public  partial class UserManager : BaseManager<User>,IUserManager
    {

	        [ImportingConstructorAttribute]
		      public UserManager( IUserRepository Repository)
		      {
                  base.GetRepository(Repository);
		      }
    }
       	
 [Export(typeof(IUser_RoleManager))]
 public  partial class User_RoleManager : BaseManager<User_Role>,IUser_RoleManager
    {

	        [ImportingConstructorAttribute]
		      public User_RoleManager( IUser_RoleRepository Repository)
		      {
                  base.GetRepository(Repository);
		      }
    }
       }
