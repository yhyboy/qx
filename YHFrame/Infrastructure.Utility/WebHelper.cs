﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Infrastructure.Utility
{
    public static class WebHelper
    {
    /// <summary>
    /// 获取所有控制器内容
    /// </summary>
    /// <param name="assemblystr">web程序集字符串</param>
    /// <returns></returns>
      public static IList<ActionPermission> GetAllActionByAssembly(string assemblystr)
      {
          var result = new List<ActionPermission>();

          var types = Assembly.Load(assemblystr).GetTypes();
          foreach (var type in types)
          {
              if (type.BaseType.Name == "Controller")//如果是Controller
              {
                  var members = type.GetMethods();
                  foreach (var member in members)
                  {
                      if (member.ReturnType.Name == "ActionResult")//如果是Action
                      {
                          var ap =new  ActionPermission();
                          ap.ActionName = member.Name;
                          ap.ControllerName = member.DeclaringType.Name.Substring(0, member.DeclaringType.Name.Length - 10); // 去掉“Controller”后缀
                          if (member.DeclaringType.FullName.Contains("Areas"))
                          {
                              string[] fullname_split = member.DeclaringType.FullName.Split('.');
                              for (int i = 0; i < fullname_split.Length; i++)
                              {
                                  if (fullname_split[i] == "Areas")
                                  {
                                      ap.Area = fullname_split[i + 1];
                                      break;
                                  }
                              }
                          }
                          else
                          {
                              ap.Area = "Empty";
                          }
                          object[] attrs = member.GetCustomAttributes(typeof(DescriptionAttribute), true);
                          if (attrs.Length > 0)
                              ap.Description = (attrs[0] as DescriptionAttribute).Description;
                          object[] attrs2 = member.GetCustomAttributes(typeof(HttpPostAttribute), true);
                          if (attrs2.Length > 0)
                              ap.Methed = "post";
                          object[] attrs3 = member.GetCustomAttributes(typeof(HttpGetAttribute), true);
                          if (attrs3.Length > 0)
                              ap.Methed = "get";
                          if (string.IsNullOrEmpty(ap.Methed))
                          {
                              ap.Methed = "get/post";
                          }
                          result.Add(ap);
                      }
                  }
              }
          }
          return result;
      }
      /// <summary>
      /// 控制器类
      /// </summary>
      public class ActionPermission
      {
          public string ActionName { get; set; }
          public string ControllerName { get; set; }
          public string Description { get; set; }
          public string Area { get; set; }

          public string Methed { get; set; }

      }

    }
}
