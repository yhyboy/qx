﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Common
{
    /// <summary>
    /// 权限类型
    /// </summary>
    public enum PermssionType
    {
        /// <summary>
        /// 模块
        /// </summary>
        Module = 1,
        /// <summary>
        /// 普通权限
        /// </summary>
        Permssion = 2,
    };
}
