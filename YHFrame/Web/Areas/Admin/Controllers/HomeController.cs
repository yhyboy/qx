﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Core;
using Model.Base;
using System.ComponentModel.Composition;

namespace Web.Areas.Admin.Controllers
{
    [Export]
    public class HomeController : Controller
    {
        public ActionResult IndexT()
        {
            return View();
        }
        public ActionResult Hello()
        {

            return PartialView();
        }
        public ActionResult Index1()
        {
            return Content("1");
        }
        public ActionResult Index2()
        {
            return Content("2");
        }
        public ActionResult Index21()
        {
            return Content("21");
        }
        public ActionResult Index22()
        {
            return Content("22");
        }
        public ActionResult Index211()
        {
            return Content("211");
        }

        public ActionResult IndexL()
        {

            return View();
        }
        public ActionResult Index()
        {

            return View();
        }
        /// <summary>
        /// 内容栏上部导航
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ContentNav(int MenuId)
        {
            ViewBag.id = MenuId;
            return PartialView();
        }


    }
}