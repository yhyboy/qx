﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Core;
using Infrastructure.Utility;
using  Model.Base;


namespace Web.Controllers
{
    [Export]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Content("");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpGet]
        [Description("这个")]
        public ActionResult Layui()
        {
            var r = WebHelper.GetAllActionByAssembly("Web");

            return Json(r,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Layui(int i)
        {

            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}