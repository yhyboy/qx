﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;
using System.Configuration;

namespace Dao
{
  public  class SqlSugarContext
    {
      private SqlSugarContext()
      {
      }
      public static string ConnectionString
      {
          get
          {
              //string reval = "server=.;uid=sa;pwd=sasa;database=SqlSugarTest";
              string reval = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
              return reval;
          }
      }
      public static SqlSugarClient GetInstance()
      {
          var db = new SqlSugarClient(ConnectionString);
          //db.IsEnableLogEvent = true;//Enable log events
         // db.LogEventStarting = (sql, par) => { Console.WriteLine(sql + " " + par + "\r\n"); };
          return db;
      }



    }
}
