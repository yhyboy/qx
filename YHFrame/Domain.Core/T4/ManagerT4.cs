﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.Base;
using Model;
using System.ComponentModel.Composition;
namespace Domain.Core
{

    public partial class UserManager : BaseManager<User>
    {

    }

    public partial class DepartmentManager : BaseManager<Department>
    {

    }

    public partial class PermssionManager : BaseManager<Permssion>
    {

    }

    public partial class Role_PermssionManager : BaseManager<Role_Permssion>
    {

    }

    public partial class RoleManager : BaseManager<Role>
    {

    }

    public partial class User_RoleManager : BaseManager<User_Role>
    {

    }

}