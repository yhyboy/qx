﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Dao;
using Model.Common;
using Model.Common;
using SqlSugar;

namespace Domain.Core
{

    public abstract class BaseManager<T> where T : class , new()
    {

        private SqlSugarClient _dbClient= SqlSugarContext.GetInstance();
        public void GetRepository()
        {
        }

        #region 查询
        /// <summary>
        /// 查询单个
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public T Find(System.Linq.Expressions.Expression<Func<T, bool>> where)
        {
            return _dbClient.Queryable<T>().SingleOrDefault(where);
        }
        /// <summary>
        /// 查询单个 必须有主键
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T Find(int id)
        {
            return _dbClient.Queryable<T>().InSingle(id);
        }
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public bool Exist(Expression<Func<T, bool>> where)
        {
            return _dbClient.Queryable<T>().Any(where);
        }
        /// <summary>
        /// 查询多个
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<T> FindList(System.Linq.Expressions.Expression<Func<T, bool>> where)
        {
            return _dbClient.Queryable<T>().Where(where).ToList();
        }

        /// <summary>
        /// 查询记录数
        /// </summary>
        /// <param name="where">条件表达式</param>
        /// <returns>记录数</returns>
        public int Count(Expression<Func<T, bool>> where)
        {
            using (_dbClient)
            {
                return _dbClient.Queryable<T>().Where(where).Count();
            }
        }

        #endregion

        #region 添加


        /// <summary>
        /// 添加单个实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Model.Common.Response Add(T entity)
        {
            Response response = new Response();
            int id = (int)_dbClient.Insert<T>(entity);
            if (id > 0)
            {
                response.Code = 1;
                response.Message = "添加数据成功！";
                response.Data = id;
            }
            else
            {
                response.Code = 0;
                response.Message = "添加数据失败！";
            }
            return response;
        }
        /// <summary>
        /// 批量添加（支持大数据）
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public Model.Common.Response Add(List<T> entities)
        {
            Response response = new Response();
            if (_dbClient.SqlBulkCopy<T>(entities))
            {
                response.Code = 1;
                response.Message = "添加数据成功！";

            }
            else
            {
                response.Code = 0;
                response.Message = "添加数据失败！";
            }
            return response;
        }
        #endregion

        #region 更新
        /// <summary>
        /// 单个更新
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Model.Common.Response Update(T entity)
        {
            Response response = new Response();
            if (_dbClient.Update(entity))
            {
                response.Code = 1;
                response.Message = "修改数据成功！";
                response.Data = entity;
            }
            else
            {
                response.Code = 0;
                response.Message = "修改数据失败！";
            }
            return response;
        }
        /// <summary>
        /// 单个更新（指定字段）
        /// </summary>
        /// <param name="model"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public Model.Common.Response Update(object model, Expression<Func<T, bool>> expression)
        {
            Response response = new Response();
            if (_dbClient.Update<T>(model, expression))
            {
                response.Code = 1;
                response.Message = "修改数据成功！";

            }
            else
            {
                response.Code = 0;
                response.Message = "修改数据失败！";
            }
            return response;
        }

        public Model.Common.Response Update(List<T> entities)
        {
            Response response = new Response();
            if (_dbClient.SqlBulkReplace(entities))
            {
                response.Code = 1;
                response.Message = "修改数据成功！";
            }
            else
            {
                response.Code = 0;
                response.Message = "修改数据失败！";
            }
            return response;
        }

        #endregion

        #region 删除

        public Model.Common.Response Delete(int id)
        {
            Response response = new Response();
            if (_dbClient.Delete<T, int>(id))
            {
                response.Code = 1;
                response.Message = "删除数据成功！";
            }
            else
            {
                response.Code = 0;
                response.Message = "删除数据失败！";
            }
            return response;
        }
        public Model.Common.Response Delete(T entity)
        {
            Response response = new Response();
            if (_dbClient.Delete<T>(entity))
            {
                response.Code = 1;
                response.Message = "删除数据成功！";
            }
            else
            {
                response.Code = 0;
                response.Message = "删除数据失败！";
            }
            return response;
        }

        public Model.Common.Response Delete(System.Linq.Expressions.Expression<Func<T, bool>> where)
        {
            Response response = new Response();
            if (_dbClient.Delete<T>(where))
            {
                response.Code = 1;
                response.Message = "删除数据成功！";
            }
            else
            {
                response.Code = 0;
                response.Message = "删除数据失败！";
            }
            return response;
        }

        public Model.Common.Response Delete(List<T> entities)
        {
            Response response = new Response();
            if (_dbClient.Delete<T>(entities))
            {
                response.Code = 1;
                response.Message = "删除数据成功！";
                response.Data = entities;
            }
            else
            {
                response.Code = 0;
                response.Message = "删除数据失败！";
            }
            return response;
        }
        #endregion

        #region 分页
        /// <summary>
        /// 查找分页数据列表 支持字符串Where
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">每页记录数</param>
        /// <param name="rows">总行数</param>
        /// <param name="totalPage">分页总数</param>
        /// <param name="whereLambda">查询表达式</param>
        /// <param name="isAsc">是否升序</param>
        /// <param name="orderBy">orderBy 拉姆达表达式</param>
        /// <returns></returns>
        public List<T> GetPageList(int pageIndex, int pageSize, out int rows, out int totalPage,
            Expression<Func<T, bool>> whereLambda, bool isAsc,
            Expression<Func<T, Object>> orderBy)
        {
         
                var temp = _dbClient.Queryable<T>().Where(whereLambda);
                rows = temp.Count();
                totalPage = rows % pageSize == 0 ? rows / pageSize : rows / pageSize + 1;
                temp = isAsc ? temp.OrderBy(orderBy) : temp.OrderBy(orderBy, OrderByType.Desc);
                return temp.Skip<T>(pageSize * (pageIndex - 1)).Take<T>(pageSize).ToList();
            
        }

        /// <summary>
        /// 查找分页数据列表
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">每页记录数</param>
        /// <param name="rows">总行数</param>
        /// <param name="totalPage">分页总数</param>
        /// <param name="whereLambda">查询表达式</param>
        /// <param name="orderBy">排序表达式</param>
        /// <returns></returns>
        public List<T> GetPageList(int pageIndex, int pageSize, out int rows, out int totalPage, Expression<Func<T, bool>> whereLambda,
            string orderBy)
        {
       
                var temp = _dbClient.Queryable<T>().Where(whereLambda);
                rows = temp.Count();
                totalPage = rows % pageSize == 0 ? rows / pageSize : rows / pageSize + 1;
                temp = temp.OrderBy(orderBy);
                return temp.Skip<T>(pageSize * (pageIndex - 1)).Take<T>(pageSize).ToList();
            
        }

        /// <summary>
        /// 查找分页数据列表 支持字符串Where
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">每页记录数</param>
        /// <param name="rows">总行数</param>
        /// <param name="totalPage">分页总数</param>
        /// <param name="whereLambda">查询表达式</param>
        /// <param name="orderBy">排序id desc,name asc</param>
        /// <returns></returns>
        public List<T> GetPageList(int pageIndex, int pageSize, out int rows, out int totalPage, string whereLambda, string orderBy)
        {
   
                var temp = _dbClient.Queryable<T>().Where(whereLambda);
                rows = temp.Count();
                totalPage = rows % pageSize == 0 ? rows / pageSize : rows / pageSize + 1;
                temp = temp.OrderBy(orderBy);
                return temp.Skip<T>(pageSize * (pageIndex - 1)).Take<T>(pageSize).ToList();
            
        }
        #endregion

    }
}
